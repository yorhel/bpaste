#!/bin/sh

# bpaste - Paste code to p.blicky.net from the commandline
#
# Copy this script to /usr/bin/bpaste and upload the output of
# any command by appending '|bpaste' to it. See -? for
# more options.
#
# Installation instructions for the lazy:
# $ sudo sh -c 'curl https://p.blicky.net/bpaste.sh -o /usr/bin/bpaste && chmod 755 /usr/bin/bpaste'
#
# This script only needs a working shell and curl.
#
# projects@yorhel.nl
# License: MIT
#
# 0.7 - 2021-04-06
# - Fix handling of lowercase Location: header
#
# 0.6 - 2018-01-21
# - Display absolute URL even when the server gives a relative redirect
#
# 0.5 - 2016-07-02
# - Fix inversion of the -c option
#
# 0.4 - 2016-06-21
# - Remove -l option
#
# 0.3 - 2016-02-14
# - Rewrote from perl to shell+curl
# - Add HTTPS support
# - Add -l option
#
# 0.2 - 2010-10-17
# - Use Getopt::Std for option parsing
# - Allow uploading a file with -f option
# - Added -c switch
# - Added passcode file
# - Use multipart/form-data for uploading
#
# 0.1 - 2010-01-21
# - Initial version

HOST=https://p.blicky.net/
SYNTAX=nosyntax
FILE=-
WRAP=0
CLICK=1
VERSION=0.7


showhelp() {
    cat <<_
$0 [-wcl] [-s <syntax>] [-f <file>] [-h <host>]
  -f <file>    Upload the contents of <file>. Default: stdin.
  -h <host>    URL to paste to. Default: https://p.blicky.net/.
  -s <syntax>  Enable syntax highlighting using <syntax>.
  -w           Allow line wrapping.
  -c           Don't make URLs clickable.

To upload pastes with a passcode, store your passcode in
  ~/.bpastepass (per user) or /etc/bpastepass (globally).
_
    exit
}


while getopts 'h:s:f:wcl?' opt; do
    case "$opt" in
        h) HOST=$OPTARG ;;
        s) SYNTAX=$OPTARG ;;
        f) FILE=$OPTARG ;;
        w) WRAP=1 ;;
        c) CLICK=0 ;;
        ?) showhelp ;;
    esac
done


# Write to a temporary file to avoid having to escape the file name given to
# curl, and to support named pipes and stuff.
TMPFILE=`mktemp`
trap "rm -f \"$TMPFILE\"" EXIT
cat "$FILE" >$TMPFILE


# Read passcode file
PASS=
[ -s /etc/bpastepass ] && PASS=`head -n 1 /etc/bpastepass`
[ -s ~/.bpastepass ] && PASS=`head -n 1 ~/.bpastepass`


curl -si -A "bpaste $VERSION" -F "f=<$TMPFILE" -F "s=$SYNTAX" -F "w=$WRAP" -F "c=$CLICK" -F "p=$PASS" $HOST\
  | grep -i 'Location: '\
  | sed 's/^[Ll]ocation: http/http/'\
  | sed "s#^[Ll]ocation: /#$HOST#"
